# Learn Maching Learning
=======================

This repository contains files and mini-projects related to exercises done during learning machine learning.

This is initially cloned from https://github.com/udacity/ud120-projects.git




## Naive Bayes

* **Python interactive mode from directory `project_root/naive_bayes`** 

```python
    from  nb_author_id import GaussianNaiveBayes 
    test1 = GaussianNaiveBayes()
    test1.train()
    test1.check_accuracy()
    test1.predict(data_to_test)
```

_During prediction the data may need to be reshaped if only one sample is predicted_


## SVM

#### **All the following are done in Python interactive mode from directory `project_root/svm`**  

* **SVC kernel = 'rbf', C = 1 and gamma = 'auto'**
```python
import svm_author_id
test2 = svm_author_id.SvmSvc()
test2.check_accuracy()
```
`training time = 2102.331 s
accuracy = 0.4920364050056883`




* **SVC kernel = 'linear', C = 1 and gamma = 'auto'**
```python
import svm_author_id
test3 = svm_author_id.SvmSvc(kernel = 'linear')
test3.check_accuracy()
```
`training time = 268.003 s
accuracy = 0.9840728100113766`


* **SVC kernel = 'linear' C = 1, gamma = 'auto' and SMALL(1/100) TRAINING DATA** 
```python
import svm_author_id
test4 = svm_author_id.SvmSvc(kernel = 'linear', 
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test4.check_accuracy()
```
`training time = 0.109 s
accuracy = 0.8845278725824801`


* **SVC kernel = 'rbf', C = 1 and gamma = 'auto' and SMALL(1/100) TRAINING DATA** 
 _**`(The following training are with small data set so that parameter optimization can be done quickly)`**_
```python
import svm_author_id
test4 = svm_author_id.SvmSvc(kernel = 'rbf', 
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test4.check_accuracy()
```
`training time = 0.125 s
accuracy = 0.6160409556313993`

* **SVC kernel = 'rbf', C = 100 and gamma = 'auto' and SMALL(1/100) TRAINING DATA**
```python
import svm_author_id
test7 = svm_author_id.SvmSvc(kernel = 'rbf', 
                                C= 100,  
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test7.check_accuracy()
```
`training time = 0.138 s
accuracy = 0.6160409556313993`

* **SVC kernel = 'rbf', C = 1000 and gamma = 'auto' and SMALL(1/100) TRAINING DATA**
```python
import svm_author_id
test7 = svm_author_id.SvmSvc(kernel = 'rbf', 
                                C= 1000,  
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test7.check_accuracy()
```
`training time = 0.127 s
accuracy = 0.8213879408418657`


* **SVC kernel = 'rbf', C = 5000 and gamma = 'auto' and SMALL(1/100) TRAINING DATA**
```python
import svm_author_id
test9 = svm_author_id.SvmSvc(kernel = 'rbf', 
                                C= 5000,  
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test9.check_accuracy()
```
`training time = 0.121 s
accuracy = 0.8993174061433447`

* **SVC kernel = 'rbf', C = 10000 and gamma = 'auto' and SMALL(1/100) TRAINING DATA**
```python
import svm_author_id
test9 = svm_author_id.SvmSvc(kernel = 'rbf', 
                                C= 10000,  
                                features_train = svm_author_id.features_train[:len(svm_author_id.features_train)/100], 
                                labels_train = svm_author_id.labels_train[:len(svm_author_id.labels_train)/100] 
                            )
test9.check_accuracy()
```
`training time = 0.121 s
accuracy = 0.8924914675767918`

**_`(Ohho, The accuracy decreases, seems like the last parameter of C = 5000 is the optimum one)`_**


* **SVC kernel = 'rbf', C = 5000 and gamma = 'auto'** 
 _**`(Using the optimum value of C found in last tests to now train with the complete data)`**_ 

```python
import svm_author_id
test12 = svm_author_id.SvmSvc(kernel = 'rbf', C=5000)
test12.check_accuracy()
```
`training time = 185.536 s
accuracy = 0.9903299203640501`





##### To Predict
```python
import svm_author_id
test = svm_author_id.SvmSvc(kernel = 'rbf', C=5000)

# single data prediction
test.predict( svm_author_id.features_test[10].reshape( 1, -1))  

# mutliple data prediction
test.predict( [svm_author_id.features_test[10], 
                svm_author_id.features_test[26], 
                svm_author_id.features_test[50]] 
                )
```

## Decision Tree

#### **All the following are done in Python interactive mode from directory `project_root/decisiona_tree`**  

* **min_samples_split=40**
```python
import dt_author_id
test1 = sdt_author_id.DecisionTreeClass(min_samples_split=40)
test1.check_accuracy()
```
`training time = 67.884 s
accuracy = .9783845278725825`


