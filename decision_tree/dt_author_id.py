#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 3 (decision tree) mini-project.

    Use a Decision Tree to identify emails from the Enron corpus by author:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###
from sklearn.tree import DecisionTreeClassifier

class DecisionTreeClass:

    # Training 
    def __init__(self, features = features_train, labels = labels_train, min_samples_split=2):
        self.clf = DecisionTreeClassifier( min_samples_split = min_samples_split )
        print 'Training now...'
        t0 = time()
        self.clf.fit(features, labels)
        print "Done. Training time:", round(time()-t0, 3), "s"

            # predict the author
    def predict( self , data=features_test ):
        t0 = time()
        result = self.clf.predict(data)
        print "prediction time:", round(time()-t0, 3), "s" 
        return result
    

    # test accuracy with data previously separated for test
    def check_accuracy( self, feature_test_data = features_test , labels_test_data = labels_test):
        return self.clf.score( feature_test_data , labels_test_data )



#########################################################
#import dt_author_id
#test1 = DecisionTreeClass(self, min_samples_split=40)


