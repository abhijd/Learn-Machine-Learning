#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels

# Separate the data into two parts. 
# The bigger chunk is used to train the classifier 
# while a small chunk is used to test the accuracy of the trained classifier
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###
# import the Support Vector Machines SVC  package
from sklearn.svm import SVC

# create the classifier
class SvmSvc:

    # train the classifier
    def __init__( self, kernel = 'rbf' , C = 1 , gamma = 'auto', features_train = features_train, labels_train = labels_train):
        self.clf = SVC( kernel = kernel, C = C , gamma = gamma )
        print 'Training now...'
        t0 = time()
        self.clf.fit(features_train, labels_train)
        print "Done. Training time:", round(time()-t0, 3), "s"

    # predict the author
    def predict( self , data=features_test ):
        t0 = time()
        result = self.clf.predict(data)
        print "prediction time:", round(time()-t0, 3), "s" 
        return result


    # test accuracy with data previously separated for test
    def check_accuracy( self, feature_test_data = features_test , labels_test_data = labels_test):
        return self.clf.score( feature_test_data , labels_test_data )


#########################################################


